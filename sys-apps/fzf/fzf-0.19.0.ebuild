# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

EGO_PN="github.com/junegunn/fzf"

EGO_VENDOR=(
	"github.com/gdamore/encoding b23993cbb635"
	"github.com/gdamore/tcell 0a0db94084df"
	"github.com/gopherjs/gopherjs d547d1d9531e"
	"github.com/jtolds/gls v4.2.1"
	"github.com/lucasb-eyer/go-colorful c900de9dbbc7"
	"github.com/mattn/go-isatty 66b8e73f3f5c"
	"github.com/mattn/go-runewidth 14207d285c6c"
	"github.com/mattn/go-shellwords v1.0.3"
	"github.com/smartystreets/assertions b2de0cb4f26d"
	"github.com/smartystreets/goconvey 044398e4856c"
	"golang.org/x/crypto 558b6879de74 github.com/golang/crypto"
	"golang.org/x/sys a5b02f93d862 github.com/golang/sys"
	"golang.org/x/text 4ee4af566555 github.com/golang/text"
)

inherit golang-vcs-snapshot golang-build

DESCRIPTION="fzf is a general-purpose command-line fuzzy finder"
HOMEPAGE="https://github.com/junegunn/fzf"
SRC_URI="https://github.com/junegunn/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz ${EGO_VENDOR_URI}"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=">=dev-lang/go-1.11"

src_install() {
	default

	dobin "fzf"
	pushd "${WORKDIR}/${P}/src/${EGO_PN}"

	doman "man/man1/fzf.1"
	doman "man/man1/fzf-tmux.1"

	# Install the vim files
	(
		insinto "/usr/share/${PN}/vim"

		doins -r "doc/"
		doins -r "plugin/"
	)

# Install the shell files
	(
		cat > "${T}/${PN}.bash" << EOF
# Auto-completion
# ---------------
[[ \\\$- == *i* ]] && source "/usr/share/${PN}/shell/completion.bash"

# Key bindings
# ------------
source "/usr/share/${PN}/shell/key-bindings.bash"
EOF

		cat > "${T}/${PN}.zsh" << EOF
# Auto-completion
# ---------------
[[ \\\$- == *i* ]] && source "/usr/share/${PN}/shell/completion.zsh"

# Key bindings
# ------------
source "/usr/share/${PN}/shell/key-bindings.zsh"
EOF
		insinto "/usr/share/${PN}/shell"

		doins "shell/completion.bash"
		doins "shell/completion.zsh"
		doins "shell/key-bindings.bash"
		doins "shell/key-bindings.zsh"
		doins "${T}/${PN}.bash"
		doins "${T}/${PN}.zsh"
	)

	popd
}
