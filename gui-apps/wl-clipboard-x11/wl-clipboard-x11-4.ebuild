# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="A wrapper to use wl-clipboard as a drop-in replacement for X11 clipboard tools"
HOMEPAGE="https://github.com/brunelli/wl-clipboard-x11"
SRC_URI="https://github.com/brunelli/${PN}/archive/v${PV}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="!!x11-misc/xsel !!x11-misc/xclip gui-apps/wl-clipboard"
RDEPEND="${DEPEND}"
BDEPEND="sys-devel/make"

pkg_setup() {
	export DESTDIR="${D}"
}

src_configure() {
	return
}

src_compile() {
	return
}

src_test() {
	return
}

src_install() {
	make install
}
